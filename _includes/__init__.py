# __init__.py

# This file contains package constants and documentation.

"""
This set of modules contains all necessary functions for the notebooks to function normally.
Each module is related to its notebook (typically by the same name) to minimize conflict.
"""
